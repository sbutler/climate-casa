import React, { useContext } from "react";
import { SpaceContext } from "../contexts/SpaceContext";
import styled from "styled-components";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

const Space = () => {
  const { currentSpace, setSpace } = useContext(SpaceContext);

  const portalStyle = {
    marginTop: "10px"
  };

  const Headline = styled.h6`
    font-weight: 100;
    font-size: 1.3rem;
    padding: 1rem;
    color: bisque;
    margin: 10px;
  `;

  const SpaceSelector = styled.nav`
    padding-bottom: 1rem;
  `;

  const SpaceInfo = styled.p`
    font-size: 2rem;
  `;

  const Intro = styled.p`
    font-size: 1.3rem;
  `;

  const roomName = styled.p`
    font-size: 1.3rem;
  `;

  const CurrentSpace = styled.span`
    color: pink;
  `;

  const Sonnet = styled.p`
    font-size: 1.3rem;
  `;

  const key = 'Home';

  return (
    <SpaceSelector>
      <span>
        <Headline style={{marginTop: "30px"}}>Video and voice chat.  No sign-up required.</Headline>
        <Headline>Join a room and get to work. </Headline>
        <SpaceInfo>
          Now in <CurrentSpace>{currentSpace}</CurrentSpace>!
        </SpaceInfo>
      </span>

      <Tabs id="controlled-tab-example" activeKey={key} onSelect={(eventKey) => setSpace(eventKey)}>
  <Tab eventKey="Onboard" title="Onboard.Earth">
    <Sonnet />
  </Tab>
  <Tab eventKey="Blockadia" title="Blockadia Messaging">
    <Sonnet />
  </Tab>
  <Tab eventKey="Project Room" title="Project Board">
    <Sonnet />
  </Tab>
</Tabs>
    </SpaceSelector>
  );
};

export default Space;
